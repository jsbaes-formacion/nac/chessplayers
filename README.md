### ChessPlayers

***

La aplicaión Chessplayers es una aplicacion web que permite el mantenimiento de una pequeña base de jugadores famosos de ajedrez. 
Este proyecto consituye el Front-End de la aplicación y se ha desarrolado con [Angular CLI](https://github.com/angular/angular-cli) version 7.2.1.

Para su correcta ejecución, la parte Backend constituida por el proyecto chessplayer-backend-apirest debe estar en ejecucíón sobre la misma máquina.

Chessplayers es un aplicación desarrollada únicamente con fines formativos sobre el desarrollo de aplicaciones con Angular y Spring Boot.


