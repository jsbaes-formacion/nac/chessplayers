/**
 *  Clase: Escuela
 *  Clase entidad que consituye el modelo de la aplicación con información
 *  sobre la escuela de ajedrez a la que pertenecen los jugadores.
  */
export class Escuela {
    id: number;
    descripcion: string;
}
