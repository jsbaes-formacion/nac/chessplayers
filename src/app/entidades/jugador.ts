import { Escuela } from './escuela';

/**
 *  Clase: Jugador
 *  Clase entidad que consituye el modelo de la aplicaion 
 *  con informaicón sobre los jugaores.
 *  Nota:
 *  Aunque el campo "nacimiento" es un fecha en el Backend,
 *  se utiliza en el FrontEnd como cadena para simplificar 
 *  su manipulación.
 */
export class Jugador {
    id: number;
    nombre: string;
    descripcion: string;
    img: string;
    nacimiento: string;
    escuela: Escuela;
}
