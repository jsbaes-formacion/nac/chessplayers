import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { asyncData, asyncError } from './async-observable-helpers';
import { EscuelasService } from './escuelas.service';
import { Escuela } from '../entidades/escuela';

describe('EscuelasService', () => {
   /** Objetos Spy que simularan la llamada al los métodos http del api */
   let httpClientSpyGet: { get: jasmine.Spy };
   let httpClientSpyPost: { post: jasmine.Spy };
   let httpClientSpyPut: { put: jasmine.Spy };
   let httpClientSpyDelete: { delete: jasmine.Spy };
   let escuelasServiceGet: EscuelasService;
   let escuelasServicePost: EscuelasService;
   let escuelasServicePut: EscuelasService;
   let escuelasServiceDelete: EscuelasService;

   /**
    *  Antes de cada test se inicializan los spy anteriores
    *  y los objetos que llemarán a los métodos del servicio
    */
   beforeEach(() => {
       httpClientSpyGet = jasmine.createSpyObj('HttpClient', ['get']);
       escuelasServiceGet = new EscuelasService(<any>httpClientSpyGet, null);
       httpClientSpyPost = jasmine.createSpyObj('HttpClient', ['post']);
       escuelasServicePost = new EscuelasService(<any>httpClientSpyPost, null);
       httpClientSpyPut = jasmine.createSpyObj('HttpClient', ['put']);
       escuelasServicePut = new EscuelasService(<any>httpClientSpyPut, null);
       httpClientSpyDelete = jasmine.createSpyObj('HttpClient', ['delete']);
       escuelasServiceDelete = new EscuelasService(<any>httpClientSpyDelete, null);
   });

   it('Método getEscuelas(): Debe devolver los escuelas resistradas', () => {
       /** Array que vamos a comprobar que devuelva el servicio */
       const escuelasData: Escuela[] = [
           {
               id: 1,
               descripcion: 'descripcion'
           },
           {
               id: 2,
               descripcion: 'descripcion'
           }
       ];

       // Le decimos al spy del GET lo que le debe devolver al servicio
       // simulando lo que le devolvería al mismo la API
       httpClientSpyGet.get.and.returnValue(of(escuelasData));

       // Llamada al servicio y comprobación del resultado esperado
       escuelasServiceGet.getEscuelas().subscribe(escuelas => expect(escuelas).toEqual(escuelasData, 'escuelas eperadas'), fail);
       expect(httpClientSpyGet.get.calls.count()).toBe(1, 'Una llamada');
   });
   it('Método getEscuelasPag(): Debe devolver las escuelas resistrados de forma paginada', () => {
       /** Array que vamos a comprobar que devuelva el servicio */
       const escuelasData: Escuela[] = [
           {
               id: 1,
               descripcion: 'descripcion',
           },
           {
               id: 2,
               descripcion: 'descripcion',
           }
       ];

       // Constante que simula lo que devuelve el API, un objeto compuesto por
       // la escuela dada de alta (jugadorData) y un mensaje de confirmación
       const respuestaPage = { content: escuelasData,
                          pageable: {},
                          totalElements: 2,
                          totalPages: 1,
                          last: true,
                          size: 5,
                          first: true,
                          sort: {},
                          numberOfElements: 2,
                          number: 0,
                          empty: false
                       };

       // Le decimos al spy del GET lo que le debe devolver al servicio
       // simulando lo que le devolvería al mismo la API
       httpClientSpyGet.get.and.returnValue(of(respuestaPage));

       // Llamada al servicio y comprobación del resultado esperado
       escuelasServiceGet.getEscuelasPag(0).subscribe(response => expect(response).toEqual(respuestaPage, 'datos eperados'));
       expect(httpClientSpyGet.get.calls.count()).toBe(1, 'Una llamada');
   });

   it('Método getEscuela: debe devolver la escuela cuyo id se le pasa como parámetro', () => {
       /** Escuela que vamos a verificar que devuelva el servicio */
       const escuelaData: Escuela = {
           id: 1,
           descripcion: 'descripcion'
       };

       // Le decimos al spy del GET lo que le debe devolver al servicio
       // simulando lo que le devolvería al mismo la API
       httpClientSpyGet.get.and.returnValue(asyncData(escuelaData));

       // Llamada al servicio y comprobación del resultado esperado
       escuelasServiceGet.getEscuela(1).subscribe(jugador => expect(jugador).toEqual(escuelaData, 'escuela eperada'), fail);
       expect(httpClientSpyGet.get.calls.count()).toBe(1, 'Una llamada');
   });

   it('Método create: Debe dar de alta una escuela con la que se le pasa como parámetro', () => {
       /** Escuela que vamos a verificar que devuelva el servicio */
       const escuelaData: Escuela = {
           id: 1,
           descripcion: 'descripcion'
       };

       // Constante que simula lo que devuelve el API, un objeto compuesto por
       // la escuela dada de alta (escuelaData) y un mensaje de confirmación
       const response = { escuela: escuelaData, mensaje: 'La escuela ha sido creada con éxito' };

       // Le decimos al spy que cuando haga el POST devuelva la constante anterior:
       httpClientSpyPost.post.and.returnValue(asyncData(response));

       // Llamada al servicio y comprobación del resultado esperado
       escuelasServicePost
           .create(escuelaData)
           .subscribe(escuela => expect(escuela).toEqual(escuelaData, 'escuela esperada'), error => expect(error.message).toBeNull);
       expect(httpClientSpyPost.post.calls.count()).toBe(1, 'Una llamada');
   });

   it('Método update: Debe actualizar el jugador que se le pasa como parámetro', () => {
       /** Escuela que vamos a verificar que devuelva el servicio */
       const escuelaData: Escuela = {
           id: 1,
           descripcion: 'descripcion'
       };

       // Constante que simula lo que devuelve el API, un objeto compuesto por
       // la escuela dada de alta (jugadorData) y un mensaje de confirmación
       const response = { escuela: escuelaData, mensaje: 'La escuela ha sido actualizada con éxito' };

       // Le decimos al spy que cuando haga el PUT devuelva la constante anterior:
       httpClientSpyPut.put.and.returnValue(asyncData(response));

       // Llamada al servicio y comprobación del resultado esperado
       escuelasServicePut
           .update(escuelaData)
           .subscribe(escuela => expect(escuela).toEqual(escuelaData, 'escuela eperada'), error => expect(error.message).toBeNull);
       expect(httpClientSpyPut.put.calls.count()).toBe(1, 'Una llamada');
   });

   it('Método delete: Debe eliminar la exuela cuyo id se le pasa como parámetro', () => {
       /** Menaje que debe devolver la API */
       const mensaje = 'La escuela ha sido eliminado con éxito';

       /** Objeto que simula lo que devuelve el API, un mensaje de confirmación */
       const response = { mensaje: 'La escuela ha sido eliminado con éxito' };

       // Le decimos al spy que cuando haga el DELETE devuelva la constante anterior:
       httpClientSpyDelete.delete.and.returnValue(asyncData(response));

       // Llamada al servicio y comprobación del resultado esperado
       escuelasServiceDelete
           .delete(1)
           .subscribe(respuesta => expect(respuesta).toEqual(response, 'mensaje eperado'), error => expect(error.message).toBeNull);
       expect(httpClientSpyDelete.delete.calls.count()).toBe(1, 'Una llamada');
   });

   it('Debe deolver un error cuando el servidor responda con un 404', () => {
       /** Objeto de error que vamos a verificar que devuelva el servicio */
       const errorResponse = new HttpErrorResponse({
           error: '404 Not Found',
           status: 404,
           statusText: 'Not Found'
       });

       // Le decimos al spy que cuando haga el GET devuelva el objeto anterior:
       httpClientSpyGet.get.and.returnValue(asyncError(errorResponse));

       // Llamada al servicio y comprobación del resultado esperado
       escuelasServiceGet
           .getEscuelas()
           .subscribe(
               escuelas => fail('Error inesperado. No se devuelven escuelas'),
               error => expect(error.message).toContain('404 Not Found')
           );
   });
});
