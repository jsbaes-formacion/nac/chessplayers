import { JugadoresService } from './jugadores.service';
import { Jugador } from '../entidades/jugador';
import { of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { asyncData, asyncError } from './async-observable-helpers';

describe('JugadoresService', () => {
    /** Objetos Spy que simularan la llamada al los métodos http del api */
    let httpClientSpyGet: { get: jasmine.Spy };
    let httpClientSpyPost: { post: jasmine.Spy };
    let httpClientSpyPut: { put: jasmine.Spy };
    let httpClientSpyDelete: { delete: jasmine.Spy };
    let jugadoresServiceGet: JugadoresService;
    let jugadoresServicePost: JugadoresService;
    let jugadoresServicePut: JugadoresService;
    let jugadoresServiceDelete: JugadoresService;

    /**
     *  Antes de cada test se inicializan los spy anteriores
     *  y los objetos que llemarán a los métodos del servicio
     */
    beforeEach(() => {
        httpClientSpyGet = jasmine.createSpyObj('HttpClient', ['get']);
        jugadoresServiceGet = new JugadoresService(<any>httpClientSpyGet, null);
        httpClientSpyPost = jasmine.createSpyObj('HttpClient', ['post']);
        jugadoresServicePost = new JugadoresService(<any>httpClientSpyPost, null);
        httpClientSpyPut = jasmine.createSpyObj('HttpClient', ['put']);
        jugadoresServicePut = new JugadoresService(<any>httpClientSpyPut, null);
        httpClientSpyDelete = jasmine.createSpyObj('HttpClient', ['delete']);
        jugadoresServiceDelete = new JugadoresService(<any>httpClientSpyDelete, null);
    });

    it('Método getJugadores(): Debe devolver los jugadores resistrados', () => {
        /** Array que vamos a comprobar que devuelva el servicio */
        const jugadoresData: Jugador[] = [
            {
                id: 1,
                nombre: 'nombre',
                descripcion: 'descripcion',
                img: 'imagen.jpg',
                nacimiento: '2018-12-15',
                escuela: {
                    "id": 1,
                    "descripcion": "Rusa"
                }
            },
            {
                id: 2,
                nombre: 'nombre',
                descripcion: 'descripcion',
                img: 'imagen.jpg',
                nacimiento: '2018-12-15',
                escuela: {
                    "id": 1,
                    "descripcion": "Rusa"
                }
            }
        ];

        // Le decimos al spy del GET lo que le debe devolver al servicio
        // simulando lo que le devolvería al mismo la API
        httpClientSpyGet.get.and.returnValue(of(jugadoresData));

        // Llamada al servicio y comprobación del resultado esperado
        jugadoresServiceGet.getJugadores().subscribe(jugadores => expect(jugadores).toEqual(jugadoresData, 'jugadores eperados'), fail);
        expect(httpClientSpyGet.get.calls.count()).toBe(1, 'Una llamada');
    });
    it('Método getJugadoresPag(): Debe devolver los jugadores resistrados de forma paginada', () => {
        /** Array que vamos a comprobar que devuelva el servicio */
        const jugadoresData: Jugador[] = [
            {
                id: 1,
                nombre: 'nombre',
                descripcion: 'descripcion',
                img: 'imagen.jpg',
                nacimiento: '2018-12-15',
                escuela: {
                    "id": 1,
                    "descripcion": "Rusa"
                }
            },
            {
                id: 2,
                nombre: 'nombre',
                descripcion: 'descripcion',
                img: 'imagen.jpg',
                nacimiento: '2018-12-15',
                escuela: {
                    "id": 1,
                    "descripcion": "Rusa"
                }
            }
        ];

        // Constante que simula lo que devuelve el API, un objeto compuesto por
        // el jugador dado de alta (jugadorData) y un mensaje de confirmación
        const respuestaPage = { content: jugadoresData,
                           pageable: {},
                           totalElements: 2,
                           totalPages: 1,
                           last: true,
                           size: 5,
                           first: true,
                           sort: {},
                           numberOfElements: 2,
                           number: 0,
                           empty: false
                        };

        // Le decimos al spy del GET lo que le debe devolver al servicio
        // simulando lo que le devolvería al mismo la API
        httpClientSpyGet.get.and.returnValue(of(respuestaPage));

        // Llamada al servicio y comprobación del resultado esperado
        jugadoresServiceGet.getJugadoresPag(0).subscribe(response => expect(response).toEqual(respuestaPage, 'datos eperados'));
        expect(httpClientSpyGet.get.calls.count()).toBe(1, 'Una llamada');
    });

    it('Método getJugador: debe devolver el jugador cuyo id se le pasa como parámetro', () => {
        /** Jugador que vamos a verificar que devuelva el servicio */

        const jugadorData: Jugador = {
            id: 1,
            nombre: 'nombre',
            descripcion: 'descripcion',
            img: 'imagen.jpg',
            nacimiento: '2018-12-15',
            escuela: {
                "id": 1,
                "descripcion": "Rusa"
            }
        };

        // Le decimos al spy del GET lo que le debe devolver al servicio
        // simulando lo que le devolvería al mismo la API
        httpClientSpyGet.get.and.returnValue(asyncData(jugadorData));

        // Llamada al servicio y comprobación del resultado esperado
        jugadoresServiceGet.getJugador(1).subscribe(jugador => expect(jugador).toEqual(jugadorData, 'jugador eperado'), fail);
        expect(httpClientSpyGet.get.calls.count()).toBe(1, 'Una llamada');
    });

    it('Método create: Debe dar de alta un jugador con el que se le pasa como parámetro', () => {
        /** Jugador que vamos a verificar que devuelva el servicio */

        const jugadorData: Jugador = {
            id: 1,
            nombre: 'nombre',
            descripcion: 'descripcion',
            img: 'imagen.jpg',
            nacimiento: '2018-12-15',
            escuela: {
                "id": 1,
                "descripcion": "Rusa"
            }
        };

        // Constante que simula lo que devuelve el API, un objeto compuesto por
        // el jugador dado de alta (jugadorData) y un mensaje de confirmación
        const response = { jugador: jugadorData, mensaje: 'El jugador ha sido creado con éxito' };

        // Le decimos al spy que cuando haga el POST devuelva la constante anterior:
        httpClientSpyPost.post.and.returnValue(asyncData(response));

        // Llamada al servicio y comprobación del resultado esperado
        jugadoresServicePost
            .create(jugadorData)
            .subscribe(jugador => expect(jugador).toEqual(jugadorData, 'jugador eperado'), error => expect(error.message).toBeNull);
        expect(httpClientSpyPost.post.calls.count()).toBe(1, 'Una llamada');
    });

    it('Método update: Debe actualizar el jugador que se le pasa como parámetro', () => {
        /** Jugador que vamos a verificar que devuelva el servicio */

        const jugadorData: Jugador = {
            id: 1,
            nombre: 'nombre',
            descripcion: 'descripcion',
            img: 'imagen.jpg',
            nacimiento: '2018-12-15',
            escuela: {
                "id": 1,
                "descripcion": "Rusa"
            }
        };

        // Constante que simula lo que devuelve el API, un objeto compuesto por
        // el juggador dado de alta (jugadorData) y un mensaje de confirmación
        const response = { jugador: jugadorData, mensaje: 'El jugador ha sido actualizado con éxito' };

        // Le decimos al spy que cuando haga el PUT devuelva la constante anterior:
        httpClientSpyPut.put.and.returnValue(asyncData(response));

        // Llamada al servicio y comprobación del resultado esperado
        jugadoresServicePut
            .update(jugadorData)
            .subscribe(jugador => expect(jugador).toEqual(jugadorData, 'jugador eperado'), error => expect(error.message).toBeNull);
        expect(httpClientSpyPut.put.calls.count()).toBe(1, 'Una llamada');
    });

    it('Método delete: Debe eliminar el jugador cuyo id se le pasa como parámetro', () => {
        /** Menaje que debe devolver la API */
        const mensaje = 'El jugador ha sido eliminado con éxito';

        /** Objeto que simula lo que devuelve el API, un mensaje de confirmación */
        const response = { mensaje: 'El jugador ha sido eliminado con éxito' };

        // Le decimos al spy que cuando haga el DELETE devuelva la constante anterior:
        httpClientSpyDelete.delete.and.returnValue(asyncData(response));

        // Llamada al servicio y comprobación del resultado esperado
        jugadoresServiceDelete
            .delete(1)
            .subscribe(respuesta => expect(respuesta).toEqual(response, 'mensaje eperado'), error => expect(error.message).toBeNull);
        expect(httpClientSpyDelete.delete.calls.count()).toBe(1, 'Una llamada');
    });

    it('Debe deolver un error cuando el servidor responda con un 404', () => {
        /** Objeto de error que vamos a verificar que devuelva el servicio */
        const errorResponse = new HttpErrorResponse({
            error: '404 Not Found',
            status: 404,
            statusText: 'Not Found'
        });

        // Le decimos al spy que cuando haga el GET devuelva el objeto anterior:
        httpClientSpyGet.get.and.returnValue(asyncError(errorResponse));

        // Llamada al servicio y comprobación del resultado esperado
        jugadoresServiceGet
            .getJugadores()
            .subscribe(
                jugadores => fail('Error inesperado. No se devuelven jugadores'),
                error => expect(error.message).toContain('404 Not Found')
            );
    });
});