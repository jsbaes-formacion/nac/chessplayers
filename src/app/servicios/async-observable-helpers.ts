import { defer } from 'rxjs';

/* Estos métodos se utilizan en los ficheros de test del proyecto
   para simular el comportamiento asíncronos de los observadores */

// #docregion async-data
/** Create async observable that emits-once and completes
 *  after a JS engine turn */
export function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}
// #enddocregion async-data

// #docregion async-error
/** Create async observable error that errors
 *  after a JS engine turn */
export function asyncError<T>(errorObject: any) {
  return defer(() => Promise.reject(errorObject));
}
