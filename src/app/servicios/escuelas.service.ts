import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { Escuela } from '../entidades/escuela';
import { Observable, throwError, of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class EscuelasService {
    private urlEndPoint = 'http://localhost:8080/api/escuelas';

    constructor(private http: HttpClient, private router: Router) {}

    /**
     * Realizar un GET a la API del BackEnd par obtener una lista de escuelas
     */
    public getEscuelas(): Observable<Escuela[]> {
        return this.http.get<Escuela[]>(this.urlEndPoint).pipe(map(response => response as Escuela[]));
    }

    /**
     * Obtiene la lista de escuelas de forma paginada
     * @param page Número de pagina de datos a obtener
     */
    public getEscuelasPag(page: number): Observable<any> {
        return this.http.get(`${this.urlEndPoint}/page/${page}`);
    }

    /**
     * Alta en la BD de un nuevo escuela.
     * @param escuela Datos de la escuela que se va dar de alta
     */
    public create(escuela: Escuela): Observable<Escuela> {
        return this.http.post<Escuela>(this.urlEndPoint, escuela).pipe(
            map((response: any) => response.escuela as Escuela), // Mapeamos el resultado a Escuela
            catchError(e => {
                // Si el Backend devuelve un código de error en el status
                if (e.error.mensaje) {
                    console.error(e.error.mensaje);
                }
                return throwError(e);
            })
        );
    }

    /**
     * Obtiene de la BD el jugdor cuyo "id" se especifica
     * @param id Campo "id" de la escuela a recuperar
     */

    public getEscuela(id: number): Observable<Escuela> {
        return this.http.get<Escuela>(`${this.urlEndPoint}/${id}`).pipe(
            catchError(e => {
                // Si la API devuelve un código de error en el status
                if (e.error.mensaje) {
                    console.error(e.error.mensaje);
                }
                return throwError(e);
            })
        );
    }

    /**
     * Actualiza un escuela en el BackEnd
     * @param escuela Datos de la escuela a actualizar
     */
    public update(escuela: Escuela): Observable<Escuela> {
        return this.http.put<Escuela>(`${this.urlEndPoint}/${escuela.id}`, escuela).pipe(
            map((response: any) => response.escuela as Escuela), // Mapeamos el resultado a Escuela
            catchError(e => {
                // Si la API devuelve un código de error en el status
                if (e.error.mensaje) {
                    console.error(e.error.mensaje);
                }
                return throwError(e);
            })
        );
    }

    /**
     * Borra un escuela de la BD.
     * @param id Campo "id" de la escuela a borrar.
     */
    public delete(id: number): Observable<any> {
        return this.http.delete<any>(`${this.urlEndPoint}/${id}`).pipe(
            catchError(e => {
                // Si el backend devuelve un código de error en el status
                if (e.error.mensaje) {
                    console.error(e.error.mensaje);
                }
                return throwError(e);
            })
        );
    }

    /**
     * Obtiene la lista de escuelas de forma paginada
     */
    public getEscuelasPdf(): Observable<any> {
        const httpOptions = {
            responseType: 'arraybuffer' as 'json'
        };
        return this.http.get(`${this.urlEndPoint}/report`, httpOptions);
    }
}
