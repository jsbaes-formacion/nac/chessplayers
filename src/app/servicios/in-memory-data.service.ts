import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Injectable } from '@angular/core';

import { UsuarioSesion } from '../cemi-core-ng/seguridad/model/usuario-sesion';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    const usuarioSesion: UsuarioSesion = {commonName:'sggsdfg',id_organigrama:'99',username:'usuario_mock',sn:'sadfsadfg3234',urlsalida:'http://urldesalida', givenName: 'usuario_mock' }

    return {usuarioSesion};
  }
}
