import { Injectable } from '@angular/core';
import { Jugador } from '../entidades/jugador';
import { Router } from '@angular/router';
import { Observable, throwError, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

/**
 * Servicio que accede al Back-End para leer y grabar
 * en la Base de Datos de jugadores.
 */
export class JugadoresService {
    // private urlEndPoint = 'https://5cc34da7968a0b001496e0a8.mockapi.io/api/players';
    private urlEndPoint = 'http://localhost:8080/api/players';

    constructor(private http: HttpClient, private router: Router) {}

    /**
     * Realizar un GET a la API del BackEnd par obtener una lista de jugadores
     */
    public getJugadores(): Observable<Jugador[]> {
        return this.http.get<Jugador[]>(this.urlEndPoint).pipe(map(response => response as Jugador[]));
    }

    /**
     * Obtiene la lista de jugadores de forma paginada
     * @param page Número de pagina de datos a obtener
     */
    public getJugadoresPag(page: number): Observable<any> {
        return this.http.get(`${this.urlEndPoint}/page/${page}`);
    }

    /**
     * Alta en la BD de un nuevo jugador.
     * @param jugador Datos del jugador que se va dar de alta
     */
    public create(jugador: Jugador): Observable<Jugador> {
        return this.http.post<Jugador>(this.urlEndPoint, jugador).pipe(
            map((response: any) => response.jugador as Jugador), // Mapeamos el resultado a Jugador
            catchError(e => {
                // Si el Backend devuelve un código de error en el status
                if (e.error.mensaje) {
                    console.error(e.error.mensaje);
                }
                return throwError(e);
            })
        );
    }

    /**
     * Obtiene de la BD el jugdor cuyo "id" se especifica
     * @param id Campo "id" del jugador a recuperar
     */

    public getJugador(id: number): Observable<Jugador> {
        return this.http.get<Jugador>(`${this.urlEndPoint}/${id}`).pipe(
            catchError(e => {
                // Si la API devuelve un código de error en el status
                if (e.error.mensaje) {
                    console.error(e.error.mensaje);
                }
                return throwError(e);
            })
        );
    }

    /**
     * Actualiza un jugador en el BackEnd
     * @param jugador Datos del jugador a actualizar
     */
    public update(jugador: Jugador): Observable<Jugador> {
        return this.http.put<Jugador>(`${this.urlEndPoint}/${jugador.id}`, jugador).pipe(
            map((response: any) => response.jugador as Jugador), // Mapeamos el resultado a Jugador
            catchError(e => {
                // Si la API devuelve un código de error en el status
                if (e.error.mensaje) {
                    console.error(e.error.mensaje);
                }
                return throwError(e);
            })
        );
    }

    /**
     * Borra un jugador de la BD.
     * @param id Campo "id" del jugador a borrar.
     */
    public delete(id: number): Observable<any> {
        return this.http.delete<any>(`${this.urlEndPoint}/${id}`).pipe(
            catchError(e => {
                // Si el backend devuelve un código de error en el status
                if (e.error.mensaje) {
                    console.error(e.error.mensaje);
                }
                return throwError(e);
            })
        );
    }

    /**
     * Obtiene el informe de jugadores
     */
    public getJugadoresPdf(): Observable<any> {
        const httpOptions = {
            responseType: 'arraybuffer' as 'json'
        };
        return this.http.get(`${this.urlEndPoint}/report`, httpOptions);
    }
}