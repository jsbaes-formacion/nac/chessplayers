import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-cemi-core-ng',
  template: `
  <div class="jumbotron">

  <h1 class="display-3">CHESSPLAYERS</h1>

  <hr class="my-4">

  <p class="lead">Algunos de los mejores jugadores de ajedrez de la historia</p>

  <span><a href="http://www.malaga.eu"><img src="assets/img/logo-ayto.svg" style="width:9em;"></a></span>
  <span><a href="http://cemi.malaga.eu"><img src="assets/img/logo_cemi.svg" style="width:9em; padding-left: 10px;"></a></span>  
  </div>
  `,
  styles: [
    '.jumbotron { text-align: center; background: none;}',
    'p.lead {margin-bottom: 60px;}',
    '.app-header h1 {display:none}'
  ]
})
export class InicioComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
