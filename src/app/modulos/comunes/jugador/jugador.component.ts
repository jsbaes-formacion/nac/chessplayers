import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JugadoresService } from '../../../servicios/jugadores.service';
import { Jugador } from '../../../../../src/app/entidades/jugador';
import { Location } from '@angular/common';

@Component({
  selector: 'app-jugador',
  templateUrl: './jugador.component.html'
})

/**
 * Componente que visualiza los datos de un jugador
 */
export class JugadorComponent implements OnInit {
    /** Objeto con los datos del jugador a visualizar */
  public jugador: Jugador;

  constructor(private activatedRoute: ActivatedRoute, 
    private jugadoresService: JugadoresService,
    private _location: Location) {}

  ngOnInit() {
      // Al arrancar el componente se cargan sus datos
    this.activatedRoute.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.jugadoresService.getJugador(id).subscribe(jugador => (this.jugador = jugador));
      }
    });
  }

  /**
   * Navega hacía la página que llamo al componente. 
   */
  public volver(): void {
      // Se utiliza _location ya que se puede llegar a este componente tanto
      // desde el mantenimiento de jugadores como desde Jugadores.
      this._location.back();
  }
}
