import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JugadorComponent } from './jugador/jugador.component';



@NgModule({
  declarations: [JugadorComponent],
  imports: [
    CommonModule
  ]
})
export class ComunesModule { }
