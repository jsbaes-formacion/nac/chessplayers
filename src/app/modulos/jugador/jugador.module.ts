import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JugadorRoutingModule } from './jugador-routing.module';
import { FormjugadoresComponent } from './formjugadores/formjugadores.component';
import { MantjugadoresComponent } from './mantjugadores/mantjugadores.component';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { RouterModule } from '@angular/router';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { PaginatorModule } from 'primeng/paginator';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { MessagesModule } from 'primeng/messages';
import { JugadoresService } from '../../servicios/jugadores.service';
import { ComunesModule } from '../comunes/comunes.module';


@NgModule({
  declarations: [FormjugadoresComponent, MantjugadoresComponent],
  imports: [
    CommonModule,
    JugadorRoutingModule,
    CardModule,
    ButtonModule,
    RouterModule,
    TableModule,
    DialogModule,
    FormsModule,
    DropdownModule,
    CalendarModule,
    InputTextModule,
    InputTextareaModule,
    PaginatorModule,
    ConfirmDialogModule,
    MessagesModule,
    ComunesModule
  ],
  providers: [ConfirmationService, JugadoresService]
})
export class JugadorModule { }
