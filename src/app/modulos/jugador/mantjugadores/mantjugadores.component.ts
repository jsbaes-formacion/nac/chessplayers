import { Component, OnInit } from '@angular/core';
import { JugadoresService } from '../../../../../src/app/servicios/jugadores.service';
import { Jugador } from '../../../../../src/app/entidades/jugador';
import Swal from 'sweetalert2';
import { ConfirmationService } from 'primeng/api';
import { Message } from 'primeng/api';



/** Constante para Swal (Sweet Alert 2) */
const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })

@Component({
    selector: 'app-mantjugadores',
    templateUrl: './mantjugadores.component.html',
    styleUrls: ['./mantjugadores.component.css']
})
/**
 * Componente que presenta los jugadores en forma de tabla
 * y permite añadir, consultar, modificar o borrar jugadores.
 */
export class MantjugadoresComponent implements OnInit {
    /*
     *  OJO!: Es necesario importar FormsModule en el modulo para poder utilizar
     *        ngModel en la parte HTML.
     */

    /** Array de mensajes */
    msgs: Message[] = [];
    /** Array de jugadores que contendrá los jugadores de cada página */
    public jugadores: Jugador[];
    /** Objeto jugador de cada fila de la tabla */
    public jugador: Jugador;
    /** Número de registros por página */
    public numRegPorPagina: number;
    /** Número total de registros */
    public totalRegistros: number;
    /** Número de página actual */
    private numPagina: number;
    /** Gestiona la fecha del p-calendar */
    public fechaJugador: Date;

    constructor(private jugadoresService: JugadoresService,
        private confirmationService: ConfirmationService) {
        this.jugador = new Jugador();
        this.numPagina = 0;
    }

    ngOnInit() { }

    /**
     *
     * @param jugador Objeto jugador a borrar
     * Versión con ConfirmDialog
     */
    delete(jugador: Jugador): void {
        this.confirmationService.confirm({
            message: 'Seguro que desea eliminar al jugador ' + jugador.nombre + ' ',
            header: '¿Está seguro?',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.jugadoresService.delete(jugador.id).subscribe(
                    response => {
                        /* Si el borrado ha ido bien, se oculta en el control tabla
                           la fila del registro borrado para evitar cargar de nuevo
                           la página de la Base de Datos */
                        this.jugadores = this.jugadores.filter(jug => jug !== jugador);
                        this.msgs = [{
                            severity: 'success', summary: 'Borrado!',
                            detail: 'Jugador ' + jugador.nombre + ' eliminado con éxito'
                        }];

                        this.jugador = null;
                    },
                    err => {
                        /* Si se han producido erroes, se muestran en la consola de errores */
                        console.error('Código de error desde el Backend: ' + err.status);
                    }
                );
            }
        });
    }

    /**
     *
     * @param jugador Objeto jugador a borrar
     * Versión con Sweet Alert
     */
    delete2(jugador: Jugador): void {
        swalWithBootstrapButtons
            .fire({
                title: '¿Está seguro?',
                text: 'Seguro que desea eliminar al jugador ' + jugador.nombre + ' ',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si, elminar!',
                cancelButtonText: 'No, cancelar!',
                reverseButtons: true
            })
            .then(result => {
                if (result.value) {
                    this.jugadoresService.delete(jugador.id).subscribe(
                        response => {
                            /* Si el borrado ha ido bien, se oculta en el control tabla
                               la fila del registro borrado para evitar cargar de nuevo
                               la página de la Base de Datos */
                            this.jugadores = this.jugadores.filter(jug => jug !== jugador);
                            swalWithBootstrapButtons.fire('Borrado!', 'Jugador ' + jugador.nombre + ' eliminado con éxito', 'success');
                            this.jugador = null;
                        },
                        err => {
                            /* Si se han producido erroes, se muestran en la consola de errores */
                            console.error('Código de error desde el Backend: ' + err.status);
                        }
                    );
                }
            });
    }


    /**
     * Método "lazy" que carga los datos paginados de la Base de Datos
     * cuando éstos son requeridos.
     * @param event Evento que se lanza cuando se requiere una nueva página de datos
     */
    public cargaDatosPaginados(event: any): void {
        /* Si estamos en la primera página, como en el caso inicial,
            el número de pagina activa, necesaria para llamar al servicio
            que trae una página concreta de la BD, es la primera (indice 0) */
        if (event.first === 0) {
            this.numPagina = 0;
        } else {
            /* Si no estamos en la primera página, la página activa se calcula a
               partir del índice del prime registro visualizado y el número de
               registros por página */
            this.numPagina = event.first / event.rows;
        }
        /* Se lee la página de datos a partir del servicio que devuelve 
          un página concreta de la Base de Datos. 
          Además de los jugadores, se lee del propio Backend el número de
          registros por páina y el total de registro existente */
        this.jugadoresService.getJugadoresPag(this.numPagina).subscribe(response => {
            this.jugadores = response.content as Jugador[];
            this.totalRegistros = response.totalElements;
            this.numRegPorPagina = response.pageable.pageSize;
        });
    }

    /**
     * Obtiene y muestra en el navegador un pdf con el listado de jugadores
     */
    public showRepJugadores() {
        this.jugadoresService.getJugadoresPdf().subscribe(response => {
            const file = new Blob([response], { type: 'application/pdf' });
            const fileURL = URL.createObjectURL(file);
            window.open(fileURL);
        });
    }
}
