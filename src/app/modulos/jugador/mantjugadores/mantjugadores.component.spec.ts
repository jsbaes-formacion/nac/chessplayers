import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MantjugadoresComponent } from './mantjugadores.component';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ButtonModule } from 'primeng/button';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';

describe('MantJugadoresComponent', () => {
  let component: MantjugadoresComponent;
  let fixture: ComponentFixture<MantjugadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MantjugadoresComponent ],
      imports: [TableModule,
        DialogModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ButtonModule,
        MessagesModule,
        MessageModule,
        ConfirmDialogModule],
        providers: [
          ConfirmationService
        ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MantjugadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Debe crearse', () => {
    expect(component).toBeTruthy();
  });
});
