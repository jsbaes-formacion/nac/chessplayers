import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MantjugadoresComponent } from './mantjugadores/mantjugadores.component';
import { JugadorComponent } from '../comunes/jugador/jugador.component';
import { FormjugadoresComponent } from './formjugadores/formjugadores.component';

const routes: Routes = [
  {
    path: '',
    component: MantjugadoresComponent,
    data: {
      title: 'Mantenimiento de Jugadores'
    }
  },
  {
    path: 'jugador/:id',
    component: JugadorComponent,
    data: {
        title: 'Ficha Jugador'
    }
  },
  {
    path: 'jugadores/form/:id',
    component: FormjugadoresComponent,
    data: {
        title: 'Edición de jugadores'
    }
  },
  {
    path: 'jugadores/form',
    component: FormjugadoresComponent,
    data: {
        title: 'Alta de jugadores'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JugadorRoutingModule {}
