import { Component, OnInit } from '@angular/core';
import { JugadoresService } from '../../../../../src/app/servicios/jugadores.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { Escuela } from '../../../../../src/app/entidades/escuela';
import { Jugador } from '../../../../../src/app/entidades/jugador';
import { EscuelasService } from '../../../servicios/escuelas.service';

@Component({
    selector: 'app-formjugadores',
    templateUrl: './formjugadores.component.html',
    styleUrls: ['./formjugadores.component.css']
})
export class FormjugadoresComponent implements OnInit {
    public jugador: Jugador;
    public newJugador: boolean;
    public fechaJugador: Date;
    public escuelas: Escuela[];

    constructor(private jugadoresService: JugadoresService,
                private escuelasService: EscuelasService,
                private router: Router, 
                private activatedRouter: ActivatedRoute) {
        this.jugador = new Jugador();
        this.newJugador = false;
    }

    ngOnInit() {
        this.cargarJugador();
        this.cargarEscuelas();
    }

    /**
     * Crea un nuevo jugador a partir del objeto this.jugador
     */
    public create(): void {
        /* El p-calendar devuelve un día menos del seleccionado, por lo que
         * hay que añadile un día */
        this.fechaJugador.setDate(this.fechaJugador.getDate() + 1);

        // Se pasa de tipo fecha a tipo string */
        this.jugador.nacimiento = this.fechaJugador.toISOString().split('T')[0];

        // Se llama al servicio para grabar el nuevo jugador
        this.jugadoresService.create(this.jugador).subscribe(
            jugador => {
                // Si ha habido éxito se vuelve al mantenimiento tras un mensaje de confirmación.
                this.router.navigate(['mantJugadores']);
                Swal.fire('Alta de jugadores', 'Jugador ' + jugador.nombre + ' grabado con éxito', 'success');
            },
            err => {
                // Si se producen errores, se muestran en la consola de errores.
                console.error('Código de error desdel el Backend: ' + err.status);
                console.error(err.error.errors);
            }
        );
    }

    /**
     * Actualiza un jugador exsitente a partir del objeto this.jugador
     */
    public update(): void {
        /* El p-calendar devuelve un día menos del seleccionado, por lo que
         * hay que añadile un día */
        this.fechaJugador.setDate(this.fechaJugador.getDate() + 1);

        // Se pasa de tipo fecha a tipo string */
        this.jugador.nacimiento = this.fechaJugador.toISOString().split('T')[0];

        // Llamada al servicio para actualizar el jugador:
        this.jugadoresService.update(this.jugador).subscribe(
            jugador => {
                // Si todo ah ido bien, se vuelve a la página de mantenimiento
                // tras un mensaje de confirmación al usuario
                this.router.navigate(['mantJugadores']);
                Swal.fire('Actualización de jugadores', 'Jugador ' + jugador.nombre + ' actualizado con éxito', 'success');
            },
            err => {
                // Si se producen errores, se muestran en la consola de errores.
                console.error('Código de error desde el Backend: ' + err.status);
            }
        );
    }

    /**
     * Carga inicial de los datos del jugador cuyo "id" se pasa en la URL
     * En caso de no haber recibido el "id", se trata de un alta.
     */
    cargarJugador(): void {
        this.activatedRouter.params.subscribe(params => {
            const id = params['id'];
            if (id) {
                this.jugadoresService.getJugador(id).subscribe(jugador => {
                    this.jugador = jugador;
                    // Se pasa a la variable de tipo fecha que va a gestionar
                    // el control de fecha el string con la fecha.
                    this.fechaJugador = new Date(this.jugador.nacimiento);
                });
            }
        });
    }

    /**
      * Carga de los escuelas a asociar a un jugador
      */
    public cargarEscuelas(): void {

        this.escuelasService.getEscuelas().subscribe(
            (result) => {
                this.escuelas = result;
            }
        );
    }

    /**
     * Función a la que se llama con el botón "Volver" del formulario
     * para volver a la página de Mantenimiento.
     */
    public volver(): void {
        this.router.navigate(['mantJugadores']);
    }
}
