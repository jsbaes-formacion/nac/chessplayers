import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormjugadoresComponent } from './formjugadores.component';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DropdownModule } from 'primeng/dropdown';

describe('FormjugadoresComponent', () => {
  let component: FormjugadoresComponent;
  let fixture: ComponentFixture<FormjugadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [RouterTestingModule, 
          FormsModule, 
          CalendarModule, 
          HttpClientTestingModule,
          DropdownModule],
      declarations: [ FormjugadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormjugadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
