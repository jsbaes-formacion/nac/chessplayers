import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaEscuelasComponent } from './lista-escuelas/lista-escuelas.component';
import { AltaEscuelaComponent } from './alta-escuela/alta-escuela.component';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { RouterModule } from '@angular/router';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { FormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';

import { EscuelaRoutingModule } from './escuela-routing.module';
import { EscuelasService } from '../../servicios/escuelas.service';


@NgModule({
  declarations: [ListaEscuelasComponent, AltaEscuelaComponent],
  imports: [
    CommonModule,
    EscuelaRoutingModule,
    MessagesModule,
    MessageModule,
    RouterModule,
    ButtonModule,
    TableModule,
    ConfirmDialogModule,
    FormsModule,
    DialogModule
  ],
  providers: [
      ConfirmationService, EscuelasService
  ]
})
export class EscuelaModule { }
