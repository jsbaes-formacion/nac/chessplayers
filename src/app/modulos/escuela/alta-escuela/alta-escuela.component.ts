import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Escuela } from '../../../../../src/app/entidades/escuela';
import { EscuelasService } from '../../../servicios/escuelas.service';
import { Message } from 'primeng/api';

@Component({
    selector: 'app-alta-escuela',
    templateUrl: './alta-escuela.component.html',
    styleUrls: ['./alta-escuela.component.css']
})
export class AltaEscuelaComponent implements OnInit {

    /** Parámetro de entrada que indica si se visualiza o no el componente */
    @Input() display: boolean;

    /** Parámetro de salida con los mensajes del componente */
    @Output() public altaRealizada = new EventEmitter<Message[]>();

    /** Objeto que contiene la nueva escuela a dar de alta */
    public escuela: Escuela;

    /** Mensajes que se van a mostrar en el componente padre (lista-escuelas) */
    public mensajes: Message[];

    constructor(private escuelasService: EscuelasService) {
        this.escuela = new Escuela();
        this.mensajes = [];
        this.altaRealizada = new EventEmitter();
    }

    ngOnInit() {
    }

    /**
     * Crea una nueva escuela a partir del objeto this.escuela
     */
    public save(): void {

        // Se llama al servicio para grabar el nuevo jugador
        this.escuelasService.create(this.escuela).subscribe(
            esc => {
                // Si todo ha ido bien, se muestra un mensaje de confirmación
                this.mensajes = [{
                    severity: 'success', summary: 'Nueva escuela dada de alta!',
                    detail: 'Escuela ' + esc.descripcion + ' grabada con éxito'
                }];
                this.altaRealizada.emit(this.mensajes);
            },
            err => {
                // Si se producen errores, se muestran en la consola de errores.
                console.error('Código de error desde el Backend: ' + err.status);
            }
        );
        this.display = false;
    }
}
