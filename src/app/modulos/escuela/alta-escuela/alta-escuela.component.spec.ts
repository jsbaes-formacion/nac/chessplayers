import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaEscuelaComponent } from './alta-escuela.component';
import { DialogModule } from 'primeng/dialog';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('AltaEscuelaComponent', () => {
  let component: AltaEscuelaComponent;
  let fixture: ComponentFixture<AltaEscuelaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaEscuelaComponent ],
      imports: [DialogModule,
        MessagesModule,
        MessageModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaEscuelaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
