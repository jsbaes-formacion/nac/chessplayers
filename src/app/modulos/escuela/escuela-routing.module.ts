import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaEscuelasComponent } from './lista-escuelas/lista-escuelas.component';


const routes: Routes = [
    {
      path: '',
      component: ListaEscuelasComponent,
      data: {
        title: 'Mantenimiento de Escuelas'
      }
    }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EscuelaRoutingModule { }