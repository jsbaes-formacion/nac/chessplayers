import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaEscuelasComponent } from './lista-escuelas.component';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ButtonModule } from 'primeng/button';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { AltaEscuelaComponent } from '../alta-escuela/alta-escuela.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';

describe('ListaEscuelasComponent', () => {
  let component: ListaEscuelasComponent;
  let fixture: ComponentFixture<ListaEscuelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListaEscuelasComponent, AltaEscuelaComponent],
      imports: [TableModule,
        DialogModule,
        MessagesModule,
        MessageModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ButtonModule,
        ConfirmDialogModule],
        providers: [
          ConfirmationService
        ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaEscuelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
