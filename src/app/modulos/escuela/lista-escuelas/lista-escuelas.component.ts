import { Component, OnInit } from '@angular/core';
import { Escuela } from '../../../../../src/app/entidades/escuela';
import { Message, ConfirmationService } from 'primeng/api';
import { EscuelasService } from '../../../servicios/escuelas.service';

@Component({
    selector: 'app-lista-escuelas',
    templateUrl: './lista-escuelas.component.html',
    styleUrls: ['./lista-escuelas.component.css'],
    providers: [EscuelasService]
})
export class ListaEscuelasComponent implements OnInit {
    /** Array de mensajes */
    public msgs: Message[] = [];
    /** Array de escuelas que contendrá los escuelas de cada página */
    public escuelas: Escuela[];
    /** Objeto escuela de cada fila de la tabla */
    public escuela: Escuela;
    /** Número de registros por página */
    public numRegPorPagina: number;
    /** Número total de registros */
    public totalRegistros: number;
    /** Número de página actual */
    private numPagina: number;
    /** Objeto para manejar la edición en la tabla de Escuelas */
    public clonedEscuelas: { [s: string]: Escuela } = {};
    /** Variable que controla que se haga visible o no el formulario de alta de escuelas */
    public altaVisible: boolean;

    constructor(private confirmationService: ConfirmationService, private escuelasService: EscuelasService) {
        this.escuela = new Escuela();
        this.numPagina = 0;
        this.altaVisible = false;
    }

    ngOnInit() {}

    /**
     *
     * @param escuela Objeto escuela a borrar
     * Versión con ConfirmDialog
     */
    delete(escuela: Escuela): void {
        this.confirmationService.confirm({
            message: 'Seguro que desea eliminar la escuela ' + escuela.descripcion + ' ',
            header: '¿Está seguro?',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.escuelasService.delete(escuela.id).subscribe(
                    response => {
                        /* Si el borrado ha ido bien, se oculta en el control tabla
                           la fila del registro borrado para evitar cargar de nuevo
                           la página de la Base de Datos */
                        this.escuelas = this.escuelas.filter(esc => esc !== escuela);
                        this.msgs = [
                            {
                                severity: 'success',
                                summary: 'Borrado!',
                                detail: 'Escuela ' + escuela.descripcion + ' eliminado con éxito'
                            }
                        ];

                        this.escuela = null;
                    },
                    err => {
                        /* Si se han producido erroes, se muestran en la consola de errores */
                        console.error('Código de error desde el Backend: ' + err.status);
                    }
                );
            }
        });
    }

    /**
     * Método "lazy" que carga los datos paginados de la Base de Datos
     * cuando éstos son requeridos.
     * @param event Evento que se lanza cuando se requiere una nueva página de datos
     */
    public cargaDatosPaginados(event: any): void {
        /* Si estamos en la primera página, como en el caso inicial,
            el número de pagina activa, necesaria para llamar al servicio
            que trae una página concreta de la BD, es la primera (indice 0) */
        if (event.first === 0) {
            this.numPagina = 0;
        } else {
            /* Si no estamos en la primera página, la página activa se calcula a
               partir del índice del prime registro visualizado y el número de
               registros por página */
            this.numPagina = event.first / event.rows;
        }
        /* Se lee la página de datos a partir del servicio que devuelve
          una página concreta de la Base de Datos. 
          Además de los escuelas, se lee del propio Backend el número de
          registros por páina y el total de registro existente */
        this.escuelasService.getEscuelasPag(this.numPagina).subscribe(response => {
            this.escuelas = response.content as Escuela[];
            this.totalRegistros = response.totalElements;
            this.numRegPorPagina = response.pageable.pageSize;
        });
    }

    /**
     * Recarga la página activa de la table
     */
    public reset() {
        this.escuelasService.getEscuelasPag(this.numPagina).subscribe(response => {
            this.escuelas = response.content as Escuela[];
            this.totalRegistros = response.totalElements;
            this.numRegPorPagina = response.pageable.pageSize;
        });
    }

    /**
     * Se llama cuando se entra en modo edición en la tabla de escuelas.
     * @param escuela Escuela que se está editando
     */
    onRowEditInit(escuela: Escuela) {
        this.clonedEscuelas[escuela.id] = { ...escuela };
    }

    /**
     * Actualiza los datos de la escuela que se está editando
     * @param escuela Escuela a actualizar
     */
    onRowEditSave(escuela: Escuela) {
        if (escuela.descripcion.length > 0) {
            delete this.clonedEscuelas[escuela.descripcion];
            this.update(escuela);
        } else {
            this.msgs = [{ severity: 'error', summary: 'Error', detail: 'La descripción es requerida' }];
        }
    }

    /**
     * Cancela la edición en curso
     * @param escuela Escuela que se estaba editando.
     * @param index Indice del array correspondiente a la página actual de la tabla de escuelas.
     */
    onRowEditCancel(escuela: Escuela, index: number) {
        this.escuelas[index] = this.clonedEscuelas[escuela.id];
        delete this.clonedEscuelas[escuela.id];
    }

    /**
     * Actualiza una escuela existente a partir del objeto 'escuela' que recibe como parámetro
     */
    public update(escuela: Escuela): void {
        // Llamada al servicio para actualizar el jugador:
        this.escuelasService.update(escuela).subscribe(
            esc => {
                // Si todo ah ido bien, se vuelve a la página de mantenimiento
                // tras un mensaje de confirmación al usuario
                this.msgs = [
                    {
                        severity: 'success',
                        summary: 'Actualizado!',
                        detail: 'Escuela ' + esc.descripcion + ' actualizada con éxito'
                    }
                ];
            },
            err => {
                // Si se producen errores, se muestran en la consola de errores.
                console.error('Código de error desde el Backend: ' + err.status);
            }
        );
    }
    /**
     * Añade una nueva escuela
     */
    public addEscuela() {
        this.altaVisible = true;
    }

    /**
     * Confirma el alta de una nueva escuela mostrando los mensajes recibidos del componente alta-escuela
     */
    public confirmaAlta(mensajes: Message[]) {
        this.msgs = mensajes;
        this.reset();
    }

    /**
     * Obtiene y muestra en el navegador un pdf con el listado de escuelas
     */
    public showRepEscuelas() {
        this.escuelasService.getEscuelasPdf().subscribe(response => {
            const file = new Blob([response], { type: 'application/pdf' });
            const fileURL = URL.createObjectURL(file);
            window.open(fileURL);
        });
    }
}
