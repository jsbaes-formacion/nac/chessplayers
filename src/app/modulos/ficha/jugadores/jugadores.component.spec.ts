import { JugadoresComponent } from './jugadores.component';
import { JugadoresService } from 'src/app/servicios/jugadores.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { JugadoresModule } from '../jugadores.module';
import { CardModule } from 'primeng/card';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('JugadoresComponent', () => {
    let componente: JugadoresComponent;
    const servicio = new JugadoresService(null, null);  // No vamos a llmar al servicio realmente
    let router: Router;  // No vamos a hacer uso del router realmente.

    beforeEach( () => {
        // Para cada prueba, creamos un nuevo componente => Aislamiento de pruebas unitarias.
        componente = new JugadoresComponent(servicio, router);
    });

    it('Init: Debe cargar los jugadores', () => {
        // Datos de prueba
        const jugadores = [{
            id: 1,
            nombre: 'Gabriel Friesen',
            descripcion: 'Bedfordshire',
            img: 'https://s3.amazonaws.com/uifaces/faces/twitter/ntfblog/128.jpg',
            nacimiento: '2018-12-15T13:22:28.005Z',
            escuela: {
                "id": 1,
                "descripcion": "Rusa"
            }
          },
          {
            id: 2,
            nombre: 'Gabriel Friesen',
            descripcion: 'Bedfordshire',
            img: 'https://s3.amazonaws.com/uifaces/faces/twitter/ntfblog/128.jpg',
            nacimiento: '2018-12-15T13:22:28.005Z',
            escuela: {
                "id": 1,
                "descripcion": "Rusa"
            }
          }];

          // Simulo (spy) el servicio para que devuelva la lista de jugadores de arriba.
          spyOn(servicio, 'getJugadores').and.callFake( () => {
              return of(jugadores);
          });

          // Cargo el camponente y verificamos que recupera del servicio los datos y los incluye
          // en su lista de jugadores
          componente.ngOnInit();
          expect (componente.jugadores.length).toBe(2);
    });
    it('Init: Debe llamar al enlace a jugador', () => {
        TestBed.configureTestingModule({
            imports: [ 
                JugadoresModule,
                HttpClientTestingModule,
                CardModule,
                RouterTestingModule.withRoutes([]),
            ]
        }).compileComponents();

        let fixture = TestBed.createComponent(JugadoresComponent);
        router = TestBed.get(Router);

        let component = fixture.componentInstance;
        let navigateSpy = spyOn(router,'navigate');

        component.verJugador(1);
        expect(navigateSpy).toHaveBeenCalledWith(['/jugador/', 1]);
    });
});
