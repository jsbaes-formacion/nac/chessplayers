import { Component, OnInit } from '@angular/core';
import { Jugador } from '../../../../../src/app/entidades/jugador';
import { JugadoresService } from '../../../servicios/jugadores.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-jugadores',
  templateUrl: './jugadores.component.html'
})

/**
 * Componente encargado de visualizar a los jugdores
 * como tarjetas con la informaciónd de cada uno.
 */
export class JugadoresComponent implements OnInit {
    /** Array de jugadores */
  public jugadores: Jugador[];

  constructor(private jugadoresService: JugadoresService,
              private router: Router) {
  }

  ngOnInit() {
    // Al activarse el componente se recuperan los jugadores
    // de la BD a través del servicio.
    this.jugadoresService.getJugadores().subscribe(
        (jugadores) => {
            this.jugadores = jugadores;
            console.log('jugadores:', this.jugadores);
        }
    );
  }

  /**
   * La aplicación navega al componente que visualiza los
   * datos de un jugador.
   * @param indice Campo "id" del jugador seleccionado
   * En la versión final del código no se utiliza esta forma ya que
   * parece coger el router-outlet principal, y no el del módulo. Por
   * esta razón se utiliza mejor el [routerLink] en el HTML.
   */
  public verJugador(indice: number) {
      this.router.navigate(['/jugador/', indice]);
  }
}
