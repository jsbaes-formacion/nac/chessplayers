import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FichaRoutingModule } from './ficha-routing.module';
import { JugadoresComponent } from './jugadores/jugadores.component';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { ComunesModule } from '../comunes/comunes.module';



@NgModule({
  declarations: [JugadoresComponent],
  imports: [
    CommonModule,
    ComunesModule,
    FichaRoutingModule,
    CardModule,
    ButtonModule
  ]
})
export class FichaModule { }
