import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JugadoresComponent } from './jugadores/jugadores.component';
import { JugadorComponent } from '../comunes/jugador/jugador.component';

const routes: Routes = [
  {
    path: '',
    component: JugadoresComponent,
    data: {
      title: 'Fichas'
    }
  },
  {
    path: 'jugador/:id',
    component: JugadorComponent,
    data: {
        title: 'Ficha Jugador'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FichaRoutingModule {}
