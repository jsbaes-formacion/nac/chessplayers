import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'inicio',
    pathMatch: 'full'
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: ''
    },
    children: [
      {
        path: 'inicio',
        loadChildren: () => import('./modulos/inicio/inicio.module').then(m => m.InicioModule)
      },
      {
        path: 'fichas',
        loadChildren: () => import('./modulos/ficha/ficha.module').then(m => m.FichaModule)
      },
      {
        path: 'mantJugadores',
        loadChildren: () => import('./modulos/jugador/jugador.module').then(m => m.JugadorModule)
      },
      {
        path: 'mantEscuelas',
        loadChildren: () => import('./modulos/escuela/escuela.module').then(m => m.EscuelaModule)
      }
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
