import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CemiCoreNgComponent } from './cemi-core-ng.component';

describe('CemiCoreNgComponent', () => {
  let component: CemiCoreNgComponent;
  let fixture: ComponentFixture<CemiCoreNgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CemiCoreNgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CemiCoreNgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
