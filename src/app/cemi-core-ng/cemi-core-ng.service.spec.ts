import { TestBed } from '@angular/core/testing';

import { CemiCoreNgService } from './cemi-core-ng.service';

describe('CemiCoreNgService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CemiCoreNgService = TestBed.get(CemiCoreNgService);
    expect(service).toBeTruthy();
  });
});
