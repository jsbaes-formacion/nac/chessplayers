import { NgModule, ModuleWithProviders } from '@angular/core';
import { CemiCoreNgComponent } from './cemi-core-ng.component';
import { AutenticacionGuard } from './seguridad/auth/autenticacion.guard';
import { HttpClientModule } from '@angular/common/http';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';



@NgModule({
  declarations: [CemiCoreNgComponent],
  imports: [
    HttpClientModule,
    LoggerModule.forRoot({level: NgxLoggerLevel.DEBUG})
  ],
  exports: [CemiCoreNgComponent]
})
export class CemiCoreNgModule {

  public static forRoot(environment: any): ModuleWithProviders {

    return {
      ngModule: CemiCoreNgModule,
      providers: [
        AutenticacionGuard,
        {
          provide: 'env', // you can also use InjectionToken
          useValue: environment
        }
      ]
    };
  }

}
