export interface UsuarioSesion {

    commonName: String,
    urlsalida: string,
    id_organigrama: String,
    username: String,
    sn: String,
    givenName:String

}
