import { Injectable } from "@angular/core";
import { finalize, tap } from "rxjs/operators";
import { NGXLogger } from "ngx-logger";
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse
} from "@angular/common/http";
import { UsuarioSesionService } from "../services/usuario-sesion.service";

// Este interceptor de HTTP sirve para insertar las credenciales de seguridad en todas las peticiones HTTP.

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private mylogger: NGXLogger,
    private usuarioService: UsuarioSesionService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    this.mylogger.log("INTERCEPTOR");
    this.mylogger.log(req);

    // Clone the request and replace the original headers with
    // cloned headers, updated with the authorization.
    const authReq = req.clone({
      headers: req.headers.set("Content-Type", "application/json"),
      withCredentials: true
    });

    // send cloned request with header to the next handler.
    //return next.handle(authReq);

    return next.handle(authReq).pipe(
      tap(
        // Succeeds when there is a response; ignore other events
        event => {},
        // Operation failed; error is an HttpErrorResponse
        error => {
          if (error.url.includes("/cas/login?service=")) {
            this.mylogger.log("Sesion Caducada");
            this.usuarioService.redirigirAlInicio();
          }
        }
      )
    );
  }
}
