import { Injectable, Inject } from '@angular/core';
import { UsuarioSesion } from '../model/usuario-sesion';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NGXLogger } from 'ngx-logger';


@Injectable({
  providedIn: 'root'
})
export class UsuarioSesionService {

  constructor(private _httpClient: HttpClient, private _logger: NGXLogger, @Inject('env') private env) {
   }

  public usuarioSesion: UsuarioSesion;


  login(): Promise<boolean> {

    if (this.usuarioSesion === undefined) {
        return this._httpClient.get<UsuarioSesion>(this.env.url_usuarioSesion, {
    withCredentials: true
  }).toPromise().then(datos => { this._logger.debug(datos); this.usuarioSesion = datos; return this.usuarioSesion != null; }, err => {
        this._logger.error(err);
        window.location.href = this.env.url_autenticacion;// 'http://www.google.es';
        return false;
      }).catch(excep => { this._logger.error(excep); return false; });
    } else {
      console.debug('Autenticacion cacheada');
      return Promise.resolve(true);
    }


  }

  // Cierra la sesión Angular y redirecciona a la url de salida indicada por CAS / E.V.A.
  logout(): void {
        
	this._logger.debug('salir de la aplicación');
        let urlSalida: string;
        // Antes de borrar el objeto usuario se hace una copia del valor de salida
        //Object.assign(this.usuarioSesion.urlsalida, urlSalida) ;
        urlSalida = this.usuarioSesion.urlsalida;
        this.usuarioSesion = undefined;
        window.location.href = urlSalida;
  }

  // Redirección al inicio utilizado sobre todo desde el AuthInterceptor en caso de caducidad de la sesión.
  redirigirAlInicio(): void {
        this.usuarioSesion = undefined;
        window.location.href = this.env.url_autenticacion;
  }



  
}
