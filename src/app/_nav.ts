import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  { title: true, name: 'Chessplayers' },
  {
    name: 'Home',
    url: '/inicio',
    icon: 'icon-layers'
  },
  {
    name: 'Fichas',
    url: '/fichas',
    icon: 'icon-layers'
  },
  {
    name: 'Mantenimiento',
    icon: 'icon-layers',
    children: [
      {
        name: 'Jugadores',
        url: '/mantJugadores',
        icon: 'icon-arrow-right'
      },
      {
        name: 'Escuelas',
        url: '/mantEscuelas',
        icon: 'icon-arrow-right'
      }
    ]
  }
];
