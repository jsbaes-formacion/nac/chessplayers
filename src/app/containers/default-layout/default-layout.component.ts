import { navItems } from '../../_nav';
import { Component, OnDestroy, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { MessageService } from 'primeng/api';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit, OnDestroy {
  public sidebarMinimized = false;
  public navItems = navItems;
  private changes: MutationObserver;
  public element: HTMLElement;

  time = new Date();
  timer;

  public version_app: String;
  public nombre: String;

  constructor(private _messageService: MessageService, @Inject(DOCUMENT) _document?: any) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }

  ngOnInit(): void {
    this.nombre = 'jsbarriga';
    this.version_app = environment.version_app;

        // Fecha en la parte derecha
        this.timer = setInterval(() => {
          this.time = new Date();
        }, 1000 * 60);

    // Muestra el mensaje al inicio
    setTimeout(() => {
      this._messageService.addAll([
        {severity: 'success', summary: '', detail: 'Bienvenido ' + this.nombre}
      ]);
    });
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
    clearInterval(this.timer);
  }

  salir() {

  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }
}
